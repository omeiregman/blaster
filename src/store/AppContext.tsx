import React, { createContext, ReactNode, useMemo, useReducer } from "react";
import { GridCellSize, GridData, GridType, RoughPatchOption } from "../types";
import {
  DEFAULT_CELL_AREA,
  DEFAULT_GRID_CELL_SIZE,
  DEFAULT_GRID_ROW_COLUMN_SIZE,
  DEFAULT_ROUGH_PATCH_OPTION,
  DEFAULT_SELECTED_GRID_ENTITY,
} from "../constants";
import {
  calculateSelectedGridTypeCount,
  initGridData,
  initGridTypeCount,
} from "../utils";
import { appReducer } from "./appReducer";
import {
  ACTION_TYPE,
  AppContextType,
  AppDispatchContextType,
  GridDataAndCountPayload,
  GridTypeCountPayload,
} from "./types.ts";

const initState = {
  gridRow: DEFAULT_GRID_ROW_COLUMN_SIZE,
  gridColumn: DEFAULT_GRID_ROW_COLUMN_SIZE,
  cellArea: DEFAULT_CELL_AREA,
  roughPatchOption: DEFAULT_ROUGH_PATCH_OPTION,
  gridCellSize: DEFAULT_GRID_CELL_SIZE,
  selectedGridType: DEFAULT_SELECTED_GRID_ENTITY,
  gridTypeCount: initGridTypeCount(
    DEFAULT_GRID_ROW_COLUMN_SIZE,
    DEFAULT_GRID_ROW_COLUMN_SIZE
  ),
  gridData: initGridData(
    DEFAULT_GRID_ROW_COLUMN_SIZE,
    DEFAULT_GRID_ROW_COLUMN_SIZE
  ),
};

export const AppContext = createContext<AppContextType | undefined>(undefined);
export const AppDispatchContext = createContext<
  AppDispatchContextType | undefined
>(undefined);

export const AppContextProvider: React.FC<{ children: ReactNode }> = ({
  children,
}) => {
  const [state, dispatch] = useReducer(appReducer, initState);

  const contextValue = useMemo(() => state, [state]);

  const dispatchFunctions: AppDispatchContextType = useMemo(
    () => ({
      setGridRow: (value: number) =>
        dispatch({
          type: ACTION_TYPE.SET_GRID_ROW,
          payload: {
            value,
            gridData: initGridData(state.gridColumn, value),
            gridTypeCount: initGridTypeCount(state.gridColumn, value),
          },
        }),

      setGridColumn: (value: number) =>
        dispatch({
          type: ACTION_TYPE.SET_GRID_COLUMN,
          payload: {
            value,
            gridData: initGridData(value, state.gridRow),
            gridTypeCount: initGridTypeCount(value, state.gridRow),
          },
        }),

      setCellArea: (payload: number) =>
        dispatch({
          type: ACTION_TYPE.SET_CELL_AREA,
          payload,
        }),

      setRoughPatchOption: (payload: RoughPatchOption) =>
        dispatch({
          type: ACTION_TYPE.SET_ROUGH_PATCH_OPTION,
          payload,
        }),

      setGridCellSize: (payload: GridCellSize) =>
        dispatch({
          type: ACTION_TYPE.SET_GRID_CELL_SIZE,
          payload,
        }),

      setSelectedGridType: (payload: GridType) =>
        dispatch({
          type: ACTION_TYPE.SET_SELECTED_GRID_TYPE,
          payload,
        }),

      setGridTypeCount: (payload: GridTypeCountPayload) =>
        dispatch({
          type: ACTION_TYPE.SET_GRID_TYPE_COUNT,
          payload: calculateSelectedGridTypeCount(
            state.gridTypeCount,
            payload.addedItem,
            payload.removedItem
          ),
        }),

      setGridData: (payload: GridData) =>
        dispatch({
          type: ACTION_TYPE.SET_GRID_DATA,
          payload,
        }),

      setGridDataAndCount: (payload: GridDataAndCountPayload) => {
        dispatch({
          type: ACTION_TYPE.SET_GRID_DATA_AND_COUNT,
          payload: {
            gridData: payload.gridData,
            gridTypeCount: payload.gridTypeCount,
          },
        });
      },

      resetGrid: () =>
        dispatch({
          type: ACTION_TYPE.RESET_GRID,
          payload: {
            gridData: initGridData(
              DEFAULT_GRID_ROW_COLUMN_SIZE,
              DEFAULT_GRID_ROW_COLUMN_SIZE
            ),
            gridTypeCount: initGridTypeCount(
              DEFAULT_GRID_ROW_COLUMN_SIZE,
              DEFAULT_GRID_ROW_COLUMN_SIZE
            ),
          },
        }),
    }),
    [state.gridColumn, state.gridRow, state.gridTypeCount]
  );

  return (
    <AppContext.Provider value={contextValue}>
      <AppDispatchContext.Provider value={dispatchFunctions}>
        {children}
      </AppDispatchContext.Provider>
    </AppContext.Provider>
  );
};
