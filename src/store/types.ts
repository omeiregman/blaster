import {
  GridTypeCount,
  GridCellSize,
  GridData,
  GridType,
  RoughPatchOption,
} from "../types.ts";

export interface AppContextType {
  cellArea: number;
  gridRow: number;
  gridColumn: number;
  roughPatchOption: RoughPatchOption;
  gridCellSize: GridCellSize;
  selectedGridType: GridType;
  gridData: GridData;
  gridTypeCount: GridTypeCount;
}

export interface AppDispatchContextType {
  setGridRow: (value: number) => void;
  setGridColumn: (value: number) => void;
  setCellArea: (value: number) => void;
  setRoughPatchOption: (option: RoughPatchOption) => void;
  setGridCellSize: (value: GridCellSize) => void;
  setSelectedGridType: (value: GridType) => void;
  setGridTypeCount: (value: GridTypeCountPayload) => void;
  setGridData: (gridData: GridData) => void;
  setGridDataAndCount: (value: GridDataAndCountPayload) => void;
  resetGrid: () => void;
}

export interface InitState {
  gridRow: number;
  gridColumn: number;
  cellArea: number;
  roughPatchOption: RoughPatchOption;
  gridCellSize: GridCellSize;
  selectedGridType: GridType;
  gridTypeCount: GridTypeCount;
  gridData: GridData;
}

export const enum ACTION_TYPE {
  SET_GRID_ROW,
  SET_GRID_COLUMN,
  SET_CELL_AREA,
  RESET_GRID,
  SET_ROUGH_PATCH_OPTION,
  SET_GRID_CELL_SIZE,
  SET_SELECTED_GRID_TYPE,
  SET_GRID_TYPE_COUNT,
  SET_GRID_DATA,
  SET_GRID_DATA_AND_COUNT,
}

export interface GridDataAndCountPayload {
  gridData: GridData;
  gridTypeCount: GridTypeCount;
}
export interface GridTypeCountPayload {
  addedItem: GridType;
  removedItem: GridType;
}

export interface GridRowColPayload extends GridDataAndCountPayload {
  value: number;
}

export interface SetGridRowAction {
  type: ACTION_TYPE.SET_GRID_ROW;
  payload: GridRowColPayload;
}

export interface SetGridColumnAction {
  type: ACTION_TYPE.SET_GRID_COLUMN;
  payload: GridRowColPayload;
}

export interface SetCellAreaAction {
  type: ACTION_TYPE.SET_CELL_AREA;
  payload: number;
}

export interface ResetGridAction {
  type: ACTION_TYPE.RESET_GRID;
  payload: GridDataAndCountPayload;
}

export interface SetRoughPatchOptionAction {
  type: ACTION_TYPE.SET_ROUGH_PATCH_OPTION;
  payload: RoughPatchOption;
}

export interface SetGridCellSizeAction {
  type: ACTION_TYPE.SET_GRID_CELL_SIZE;
  payload: GridCellSize;
}

export interface SetSelectedGridTypeAction {
  type: ACTION_TYPE.SET_SELECTED_GRID_TYPE;
  payload: GridType;
}

export interface SetGridTypeCountAction {
  type: ACTION_TYPE.SET_GRID_TYPE_COUNT;
  payload: GridTypeCount;
}

export interface SetGridDataAction {
  type: ACTION_TYPE.SET_GRID_DATA;
  payload: GridData;
}

export interface SetGridDataAndCountAction {
  type: ACTION_TYPE.SET_GRID_DATA_AND_COUNT;
  payload: GridDataAndCountPayload;
}

export type ReducerAction =
  | SetGridRowAction
  | SetGridColumnAction
  | SetCellAreaAction
  | ResetGridAction
  | SetRoughPatchOptionAction
  | SetGridCellSizeAction
  | SetSelectedGridTypeAction
  | SetGridTypeCountAction
  | SetGridDataAction
  | SetGridDataAndCountAction;
