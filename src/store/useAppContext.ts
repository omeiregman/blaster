import { useContext } from "react";
import { AppContext, AppDispatchContext } from "./AppContext.tsx";

export const useAppContext = () => {
  const context = useContext(AppContext);
  if (!context)
    throw new Error("useAppContext must be used within a AppContextProvider");
  return context;
};

export const useAppDispatchContext = () => {
  const context = useContext(AppDispatchContext);
  if (!context)
    throw new Error(
      "useAppDispatchContext must be used within a AppContextProvider"
    );
  return context;
};
