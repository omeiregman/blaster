import { ACTION_TYPE, InitState, ReducerAction } from "./types.ts";
import {
  DEFAULT_CELL_AREA,
  DEFAULT_GRID_CELL_SIZE,
  DEFAULT_GRID_ROW_COLUMN_SIZE,
  DEFAULT_ROUGH_PATCH_OPTION,
  DEFAULT_SELECTED_GRID_ENTITY,
} from "../constants.ts";

export const appReducer = (
  state: InitState,
  action: ReducerAction
): InitState => {
  switch (action.type) {
    case ACTION_TYPE.SET_GRID_ROW:
      return {
        ...state,
        gridRow: action.payload.value,
        gridData: action.payload.gridData,
        gridTypeCount: action.payload.gridTypeCount,
      };
    case ACTION_TYPE.SET_GRID_COLUMN:
      return {
        ...state,
        gridColumn: action.payload.value,
        gridData: action.payload.gridData,
        gridTypeCount: action.payload.gridTypeCount,
      };
    case ACTION_TYPE.SET_CELL_AREA:
      return {
        ...state,
        cellArea: action.payload,
      };
    case ACTION_TYPE.RESET_GRID:
      return {
        ...state,
        gridRow: DEFAULT_GRID_ROW_COLUMN_SIZE,
        gridColumn: DEFAULT_GRID_ROW_COLUMN_SIZE,
        cellArea: DEFAULT_CELL_AREA,
        selectedGridType: DEFAULT_SELECTED_GRID_ENTITY,
        roughPatchOption: DEFAULT_ROUGH_PATCH_OPTION,
        gridCellSize: DEFAULT_GRID_CELL_SIZE,
        gridData: action.payload.gridData,
        gridTypeCount: action.payload.gridTypeCount,
      };
    case ACTION_TYPE.SET_ROUGH_PATCH_OPTION:
      return {
        ...state,
        roughPatchOption: action.payload,
      };
    case ACTION_TYPE.SET_GRID_CELL_SIZE:
      return {
        ...state,
        gridCellSize: action.payload,
      };
    case ACTION_TYPE.SET_SELECTED_GRID_TYPE:
      return {
        ...state,
        selectedGridType: action.payload,
      };
    case ACTION_TYPE.SET_GRID_TYPE_COUNT:
      return {
        ...state,
        gridTypeCount: action.payload,
      };
    case ACTION_TYPE.SET_GRID_DATA:
      return {
        ...state,
        gridData: action.payload,
      };
    case ACTION_TYPE.SET_GRID_DATA_AND_COUNT:
      return {
        ...state,
        gridData: action.payload.gridData,
        gridTypeCount: action.payload.gridTypeCount,
      };
    default:
      return state;
  }
};
