export enum GridType {
  EMPTY = "empty",
  BUILDING = "building",
  ROAD_BLOCK = "road_block",
  ROUGH_PATCH = "rough_patch",
  ROUGH_PATCH_PASS = "rough_patch_pass",
}

export enum GridCellSize {
  XS = 0,
  SM = 25,
  MD = 50,
  LG = 75,
  XL = 100,
}

export type GridTypeCount = Record<GridType, number>;

export interface GridSquare {
  type: GridType;
  isPath: boolean;
}

export type GridData = GridSquare[][];

export type Point = [number, number];

export interface QueueItem {
  point: Point;
  path: Point[];
}

export enum RoughPatchOption {
  MIXED = "mixed",
  UNBLOCKED = "unblocked",
  BLOCKED = "blocked",
}
