import { HomePage } from "./pages/HomePage.tsx";

import "@mantine/core/styles.css";

import { MantineProvider } from "@mantine/core";
import { AppContextProvider } from "./store/AppContext.tsx";
function App() {
  return (
    <MantineProvider>
      <AppContextProvider>
        <HomePage />
      </AppContextProvider>
    </MantineProvider>
  );
}

export default App;
