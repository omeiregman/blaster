import {
  GridTypeCount,
  GridData,
  GridType,
  Point,
  QueueItem,
  RoughPatchOption,
} from "../types.ts";
import {
  DEFAULT_BUILDING_COUNT,
  MINIMUM_BUILDING_COUNT,
} from "../constants.ts";

export const initGridData = (gridRow: number, gridColumn: number): GridData => {
  return Array.from({ length: gridRow }, () =>
    Array.from({ length: gridColumn }, () => ({
      type: GridType.EMPTY,
      isPath: false,
    }))
  );
};

export const updateGridCell = (
  row: number,
  col: number,
  gridType: GridType,
  gridData: GridData
): GridData => {
  const newGrid = gridData.map((row) => row.map((cell) => cell));

  newGrid[col][row] = {
    ...newGrid[col][row],
    type: gridType,
    isPath: false,
  };
  return newGrid;
};

const isBlockedOrPatchSelected = (addedItem: GridType, removedItem: GridType) =>
  addedItem === GridType.ROUGH_PATCH ||
  addedItem === GridType.ROAD_BLOCK ||
  addedItem === GridType.ROUGH_PATCH_PASS ||
  removedItem === GridType.ROUGH_PATCH ||
  removedItem === GridType.ROAD_BLOCK ||
  removedItem === GridType.ROUGH_PATCH_PASS;

export const calculateSelectedGridTypeCount = (
  gridTypeCount: GridTypeCount,
  addedItem: GridType,
  removedItem: GridType
) => {
  const newGridTypeCount = { ...gridTypeCount };

  const blockedOrPatchType = [
    GridType.ROUGH_PATCH,
    GridType.ROAD_BLOCK,
    GridType.ROUGH_PATCH_PASS,
  ];

  if (removedItem !== GridType.EMPTY) {
    newGridTypeCount[removedItem] = (newGridTypeCount[removedItem] || 0) + 1;
  }
  if (addedItem !== GridType.EMPTY && newGridTypeCount[addedItem] > 0) {
    newGridTypeCount[addedItem]--;
  }

  const hasBlockedOrPatch = isBlockedOrPatchSelected(addedItem, removedItem);

  if (hasBlockedOrPatch) {
    const countValues = blockedOrPatchType.map(
      (type) => newGridTypeCount[type] || 0
    );
    const synchronizedCount =
      removedItem !== GridType.EMPTY
        ? Math.max(...countValues)
        : Math.min(...countValues);

    blockedOrPatchType.forEach((type) => {
      newGridTypeCount[type] = synchronizedCount;
    });
  }

  return newGridTypeCount;
};

export const initGridTypeCount = (gridRow: number, gridColumn: number) => {
  const totalGridItems = gridRow * gridColumn;

  return {
    [GridType.EMPTY]: totalGridItems,
    [GridType.BUILDING]: DEFAULT_BUILDING_COUNT,
    [GridType.ROUGH_PATCH]: totalGridItems - DEFAULT_BUILDING_COUNT,
    [GridType.ROUGH_PATCH_PASS]: totalGridItems - DEFAULT_BUILDING_COUNT,
    [GridType.ROAD_BLOCK]: totalGridItems - DEFAULT_BUILDING_COUNT,
  };
};

const isGridTypePassable = (gridType: GridType) =>
  gridType === GridType.EMPTY ||
  gridType === GridType.BUILDING ||
  gridType === GridType.ROUGH_PATCH_PASS;

export const resetShortestPath = (gridData: GridData): GridData => {
  return gridData.map((row) =>
    row.map((cell) => ({
      ...cell,
      isPath: false,
    }))
  );
};

const mapShortestPathToGrid = (gridData: GridData, path: Point[]): GridData => {
  if (path.length > 1) {
    for (const item of path) {
      const [row, col] = item;
      gridData[row][col].isPath = true;
    }
  }

  return [...gridData];
};

export const findShortestPathToBuilding = (
  gridData: GridData,
  cellArea: number
):
  | {
      gridWithShortestPath: GridData;
      pathDistance: number;
    }
  | undefined => {
  const rows = gridData.length;
  const columns = gridData[0].length;
  const directions: Point[] = [
    [0, 1],
    [1, 0],
    [0, -1],
    [-1, 0],
  ];
  let start: Point | null = null;
  let end: Point | null = null;

  for (let i = 0; i < rows; i++) {
    for (let j = 0; j < columns; j++) {
      if (gridData[i][j].type === GridType.BUILDING) {
        if (!start) {
          start = [i, j];
        } else {
          end = [i, j];
          break;
        }
      }
    }
    if (end) break;
  }

  if (!start || !end) return;

  const queue: QueueItem[] = [{ point: start, path: [start] }];
  const visited = new Set<string>([`${start[0]},${start[1]}`]);

  while (queue.length > 0) {
    const { point, path } = queue.shift()!;
    const [currentRow, currentCol] = point;

    for (const [dr, dc] of directions) {
      const nextRow = currentRow + dr;
      const nextCol = currentCol + dc;
      const nextPoint: Point = [nextRow, nextCol];
      const key = `${nextRow},${nextCol}`;

      if (
        nextRow >= 0 &&
        nextRow < rows &&
        nextCol >= 0 &&
        nextCol < columns &&
        isGridTypePassable(gridData[nextRow][nextCol].type)
      ) {
        if (!visited.has(key)) {
          visited.add(key);
          const newPath = [...path, nextPoint];

          if (nextRow === end[0] && nextCol === end[1]) {
            return {
              gridWithShortestPath: mapShortestPathToGrid(gridData, newPath),
              pathDistance: calculatePathDistance(cellArea, newPath),
            };
          }

          queue.push({ point: nextPoint, path: newPath });
        }
      }
    }
  }

  return;
};

export const calculatePathDistance = (cellArea: number, path: Point[]) => {
  const cellLength = cellArea / 2;
  return (path.length - MINIMUM_BUILDING_COUNT + 1) * cellLength;
};

export const updateRoughPatchInGrid = (
  gridData: GridData,
  roughPatchType: RoughPatchOption.BLOCKED | RoughPatchOption.UNBLOCKED
): GridData => {
  for (const item of gridData) {
    for (const square of item) {
      if (
        square.type === GridType.ROUGH_PATCH &&
        roughPatchType === RoughPatchOption.UNBLOCKED
      ) {
        square.type = GridType.ROUGH_PATCH_PASS;
      } else if (
        square.type === GridType.ROUGH_PATCH_PASS &&
        roughPatchType === RoughPatchOption.BLOCKED
      ) {
        square.type = GridType.ROUGH_PATCH;
      }
    }
  }

  return gridData;
};
