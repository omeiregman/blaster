import {
  initGridData,
  updateGridCell,
  calculateSelectedGridTypeCount,
  initGridTypeCount,
  resetShortestPath,
  calculatePathDistance,
  updateRoughPatchInGrid,
  findShortestPathToBuilding,
} from "./utils";
import { GridTypeCount, GridType, RoughPatchOption } from "../types";
import { DEFAULT_GRID_ROW_COLUMN_SIZE } from "../constants.ts";

describe("utils", () => {
  describe("initGridData", () => {
    it("should initialize a grid with the given dimensions and all cells set to empty and not part of a path", () => {
      const grid = initGridData(3, 3);
      expect(grid).toHaveLength(3);
      grid.forEach((row) => {
        expect(row).toHaveLength(3);
        row.forEach((cell) => {
          expect(cell.type).toBe(GridType.EMPTY);
          expect(cell.isPath).toBe(false);
        });
      });
    });
  });

  describe("updateGridCell", () => {
    it("should update the type of a specific cell in the grid", () => {
      const initialGrid = initGridData(3, 3);
      const updatedGrid = updateGridCell(1, 1, GridType.BUILDING, initialGrid);
      expect(updatedGrid[1][1].type).toBe(GridType.BUILDING);
    });
  });

  describe("calculateSelectedGridTypeCount", () => {
    const initialState = initGridTypeCount(
      DEFAULT_GRID_ROW_COLUMN_SIZE,
      DEFAULT_GRID_ROW_COLUMN_SIZE
    );

    test("should decrement the count of an added grid type", () => {
      const result = calculateSelectedGridTypeCount(
        initialState,
        GridType.BUILDING,
        GridType.EMPTY
      );
      expect(result[GridType.BUILDING]).toBe(1);
    });

    test("should increment the count of a removed grid type", () => {
      const result = calculateSelectedGridTypeCount(
        initialState,
        GridType.EMPTY,
        GridType.ROAD_BLOCK
      );
      expect(result[GridType.ROAD_BLOCK]).toBe(24);
    });

    test("should synchronize BLOCKED, ROUGH_PATCH and ROUGH_PATCH_PASS types to the highest count when one is removed", () => {
      const modifiedState = { ...initialState, [GridType.ROAD_BLOCK]: 3 };
      const result = calculateSelectedGridTypeCount(
        modifiedState,
        GridType.EMPTY,
        GridType.ROAD_BLOCK
      );

      expect(result[GridType.ROAD_BLOCK]).toBe(23);
      expect(result[GridType.ROUGH_PATCH]).toBe(23);
      expect(result[GridType.ROUGH_PATCH_PASS]).toBe(23);
    });

    test("should synchronize BLOCKED, ROUGH_PATCH and ROUGH_PATCH_PASS types to the lowest count when one is added", () => {
      const result = calculateSelectedGridTypeCount(
        initialState,
        GridType.ROUGH_PATCH,
        GridType.EMPTY
      );

      expect(result[GridType.ROUGH_PATCH]).toBe(22);
      expect(result[GridType.ROAD_BLOCK]).toBe(22);
      expect(result[GridType.ROUGH_PATCH_PASS]).toBe(22);
    });

    test("should handle adding and removing EMPTY type correctly", () => {
      const result = calculateSelectedGridTypeCount(
        initialState,
        GridType.EMPTY,
        GridType.EMPTY
      );
      expect(result).toEqual(initialState);
    });

    test("should not decrement below 0", () => {
      const lowState = { ...initialState, [GridType.BUILDING]: 0 };
      const result = calculateSelectedGridTypeCount(
        lowState,
        GridType.BUILDING,
        GridType.EMPTY
      );
      expect(result[GridType.BUILDING]).toBe(0);
    });

    test("should initialize undefined count to 1 when removing a grid type not present", () => {
      const result = calculateSelectedGridTypeCount(
        {} as GridTypeCount,
        GridType.EMPTY,
        GridType.BUILDING
      );
      expect(result[GridType.BUILDING]).toBe(1);
    });
  });

  describe("initGridTypeCount", () => {
    it("should initialize entity counts based on grid size", () => {
      const entityCounts = initGridTypeCount(5, 5);
      expect(entityCounts[GridType.EMPTY]).toBe(25);
      expect(entityCounts[GridType.BUILDING]).toBe(2);
    });
  });

  describe("resetShortestPath", () => {
    it("should reset the isPath flag for all cells in the grid", () => {
      const grid = initGridData(3, 3);
      grid[0][0].isPath = true;
      const resetGrid = resetShortestPath(grid);
      expect(resetGrid[0][0].isPath).toBe(false);
    });
  });

  describe("calculatePathDistance", () => {
    it("should calculate the distance of a path", () => {
      const distance = calculatePathDistance(250, [
        [0, 0],
        [0, 1],
      ]);
      expect(distance).toBe(125);
    });
  });

  describe("updateRoughPatchInGrid", () => {
    it("should update rough patches in the grid based on the given option", () => {
      const grid = initGridData(2, 2);
      grid[0][0].type = GridType.ROUGH_PATCH;
      const updatedGrid = updateRoughPatchInGrid(
        grid,
        RoughPatchOption.UNBLOCKED
      );
      expect(updatedGrid[0][0].type).toBe(GridType.ROUGH_PATCH_PASS);
    });
  });

  describe("findShortestPathToBuilding", () => {
    it("should return undefined for grids with no buildings", () => {
      const grid = initGridData(5, 5);
      const result = findShortestPathToBuilding(grid, 250);
      expect(result).toBeUndefined();
    });

    it("should return undefined for grids with only one building", () => {
      const grid = initGridData(5, 5);
      grid[2][2].type = GridType.BUILDING;
      const result = findShortestPathToBuilding(grid, 250);
      expect(result).toBeUndefined();
    });

    it("should find a direct path between two buildings", () => {
      const grid = initGridData(5, 5);
      grid[0][0].type = GridType.BUILDING;
      grid[0][4].type = GridType.BUILDING;
      const result = findShortestPathToBuilding(grid, 250);
      expect(result).toBeDefined();
      expect(result?.pathDistance).toBe(500);
      expect(result?.gridWithShortestPath[0][1].isPath).toBe(true);
      expect(result?.gridWithShortestPath[0][2].isPath).toBe(true);
      expect(result?.gridWithShortestPath[0][3].isPath).toBe(true);
    });

    it("should find a path between two buildings navigating around an obstacle", () => {
      const grid = initGridData(5, 5);
      grid[0][0].type = GridType.BUILDING;
      grid[0][3].type = GridType.BUILDING;
      grid[0][1].type = GridType.ROAD_BLOCK;
      const result = findShortestPathToBuilding(grid, 250);
      expect(result).toBeDefined();
      expect(result?.gridWithShortestPath[0][1].isPath).toBe(false);
      expect(result?.gridWithShortestPath[1][1].isPath).toBe(true);
      expect(result?.gridWithShortestPath[1][0].isPath).toBe(true);
      expect(result?.gridWithShortestPath[1][2].isPath).toBe(true);
      expect(result?.gridWithShortestPath[1][3].isPath).toBe(true);
    });

    it("should return undefined if no path exists between two buildings", () => {
      const grid = initGridData(5, 5);
      grid[0][0].type = GridType.BUILDING;
      grid[4][4].type = GridType.BUILDING;
      for (let i = 0; i < 5; i++) {
        grid[2][i].type = GridType.ROAD_BLOCK;
      }
      const result = findShortestPathToBuilding(grid, 250);
      expect(result).toBeUndefined();
    });
  });
});
