import { Grid, SettingsPanel } from "../features";

export const HomePage = () => {
  return (
    <div className="flex overflow-auto w-max">
      <div className="sm:fixed sm:top-0 sm:left-0 h-auto sm:h-full w-80 bg-blue-100 overflow-y-scroll">
        <SettingsPanel />
      </div>
      <div className="flex-1 sm:pl-80 w-full h-full">
        <Grid />
      </div>
    </div>
  );
};
