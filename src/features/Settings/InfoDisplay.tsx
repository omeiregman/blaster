import { List, Notification, Paper, rem, ThemeIcon } from "@mantine/core";
import { IconCircleDashed } from "@tabler/icons-react";
import React, { ReactNode } from "react";

export const InfoDisplay: React.FC<{
  pathDistance?: number;
  error?: string;
}> = ({ pathDistance, error }) => {
  return (
    <div className="px-4 py-2">
      <Paper shadow="lg" px="md" py="xs" radius="xs">
        <List>
          <ListItem>
            Select a Grid type and click on empty grid cell to add
          </ListItem>
          <ListItem>
            To clear item from the grid, unselect grid type and click the grid
            that needs to be cleared
          </ListItem>
          <ListItem>After each calculation, reset the grid</ListItem>
        </List>
        {(pathDistance ?? error) && (
          <div className="px-4">
            <Notification
              withCloseButton={false}
              color={error ? "red" : "green"}
            >
              {pathDistance && (
                <div className="text-sm font-bold">{`Total Path Distance: ${pathDistance?.toString()}m`}</div>
              )}
              {error && (
                <div className="text-sm font-bold text-red-700">{error}</div>
              )}
            </Notification>
          </div>
        )}
      </Paper>
    </div>
  );
};

const ListItem: React.FC<{ children: ReactNode }> = ({ children }) => (
  <List.Item
    icon={
      <ThemeIcon color="blue" size={24} radius="lg">
        <IconCircleDashed style={{ width: rem(16), height: rem(12) }} />
      </ThemeIcon>
    }
    className="pb-2 text-xs"
  >
    {children}
  </List.Item>
);
