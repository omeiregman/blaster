import React from "react";

import { Switch, useMantineTheme, rem } from "@mantine/core";
import { IconCheck, IconX } from "@tabler/icons-react";

interface RoughPatchToggleProps {
  onToggle: (isRoughPatchPass: boolean) => void;
  disabled: boolean;
  checked: boolean;
}
export const RoughPatchToggle: React.FC<RoughPatchToggleProps> = ({
  onToggle,
  disabled,
  checked,
}) => {
  const theme = useMantineTheme();

  const handleToggle = (event: React.ChangeEvent<HTMLInputElement>) => {
    onToggle(event.currentTarget.checked);
  };

  return (
    <div className="pl-3 flex">
      <Switch
        className="border-2 border-gray-500 rounded-full"
        checked={checked}
        onChange={handleToggle}
        size="sm"
        disabled={disabled}
        thumbIcon={
          checked ? (
            <IconCheck
              style={{ width: rem(12), height: rem(12) }}
              color={theme.colors.teal[6]}
              stroke={3}
            />
          ) : (
            <IconX
              style={{ width: rem(12), height: rem(12) }}
              color={theme.colors.red[6]}
              stroke={3}
            />
          )
        }
        color="dark.4"
      />
    </div>
  );
};
