import {
  Button,
  Divider,
  NumberInput,
  Select,
  Slider,
  Text,
} from "@mantine/core";
import BuildingStart from "../../assets/building-start.svg";
import RoadBlock from "../../assets/road-block.svg";
import RoughPatch from "../../assets/rough-patch.svg";
import RoughPatchFree from "../../assets/rough-patch-free.svg";
import React, { useState } from "react";
import { AddGridItem } from "./AddGridItem.tsx";

import { GridCellSize, GridType, RoughPatchOption } from "../../types.ts";
import {
  findShortestPathToBuilding,
  updateRoughPatchInGrid,
} from "../../utils";
import {
  DEFAULT_BUILDING_COUNT,
  GRID_SIZE_MARKS,
  MINIMUM_BUILDING_COUNT,
  ROUGH_PATCH_OPTIONS,
} from "../../constants.ts";
import { RoughPatchToggle } from "./RoughPatchToggle.tsx";
import { InfoDisplay } from "./InfoDisplay.tsx";
import {
  useAppContext,
  useAppDispatchContext,
} from "../../store/useAppContext.ts";

export const SettingsPanel: React.FC = () => {
  const [isShortestPathCalculated, setIsShortestPathCalculated] =
    useState(false);
  const [pathDistance, setPathDistance] = useState<number | undefined>();
  const [error, setError] = useState<string | undefined>();

  const {
    gridRow,
    gridColumn,
    selectedGridType,
    gridTypeCount,
    gridData,
    cellArea,
    gridCellSize,
    roughPatchOption,
  } = useAppContext();

  const {
    setCellArea,
    setGridColumn,
    setGridRow,
    setGridCellSize,
    setRoughPatchOption,
    setSelectedGridType,
    setGridData,
    resetGrid,
  } = useAppDispatchContext();

  const handleReset = () => {
    resetGrid();
    setPathDistance(undefined);
    setError(undefined);
    setIsShortestPathCalculated(false);
  };

  const isCalculateDisabled =
    isShortestPathCalculated ||
    gridTypeCount[GridType.BUILDING] !==
      DEFAULT_BUILDING_COUNT - MINIMUM_BUILDING_COUNT;

  const calculateShortestPath = () => {
    const { gridWithShortestPath: newGrid, pathDistance } =
      findShortestPathToBuilding(gridData, cellArea) ?? {};
    if (newGrid && newGrid.length !== 0) {
      setGridData(newGrid);
      setPathDistance(pathDistance);
      setIsShortestPathCalculated(true);
      setError(undefined);
    } else {
      setError("No Valid Path found");
    }
  };

  const [roughPatchType, setRoughPatchType] = useState(GridType.ROUGH_PATCH);

  const handleRoughPatchToggle = (isRoughPatchPass: boolean) => {
    const roughPatch = isRoughPatchPass
      ? GridType.ROUGH_PATCH_PASS
      : GridType.ROUGH_PATCH;
    setSelectedGridType(roughPatch);
    setRoughPatchType(roughPatch);
  };

  const setRoughPatchInGrid = (roughPatchSelection: RoughPatchOption) => {
    setRoughPatchOption(roughPatchSelection);
    if (roughPatchSelection === RoughPatchOption.BLOCKED) {
      setRoughPatchType(GridType.ROUGH_PATCH);
    }
    if (roughPatchSelection === RoughPatchOption.UNBLOCKED) {
      setRoughPatchType(GridType.ROUGH_PATCH_PASS);
    }

    if (roughPatchSelection !== RoughPatchOption.MIXED) {
      const updatedGridData = updateRoughPatchInGrid(
        gridData,
        roughPatchSelection
      );
      setGridData(updatedGridData);
    }
    return;
  };

  const disabledRoughPatchToggle =
    roughPatchOption !== RoughPatchOption.MIXED ||
    !(
      selectedGridType === GridType.ROUGH_PATCH_PASS ||
      selectedGridType === GridType.ROUGH_PATCH
    );

  return (
    <div className="flex flex-col space-y-4">
      <InfoDisplay pathDistance={pathDistance} error={error} />
      <Button
        variant="transparent"
        color="red"
        onClick={handleReset}
        className="underline w-28"
      >
        Reset Grid
      </Button>
      <div className="flex flex-col">
        <AddGridItem
          item={GridType.BUILDING}
          image={BuildingStart}
          totalLeft={gridTypeCount[GridType.BUILDING]}
          color="purple"
          selectedGridType={selectedGridType}
          setSelectedGridType={setSelectedGridType}
        />
        <AddGridItem
          item={roughPatchType}
          image={
            roughPatchType === GridType.ROUGH_PATCH
              ? RoughPatch
              : RoughPatchFree
          }
          totalLeft={gridTypeCount[GridType.ROUGH_PATCH]}
          color="yellow"
          selectedGridType={selectedGridType}
          setSelectedGridType={setSelectedGridType}
          toggle={
            <RoughPatchToggle
              onToggle={handleRoughPatchToggle}
              disabled={disabledRoughPatchToggle}
              checked={roughPatchType === GridType.ROUGH_PATCH_PASS}
            />
          }
        />
        <AddGridItem
          item={GridType.ROAD_BLOCK}
          image={RoadBlock}
          totalLeft={gridTypeCount[GridType.ROAD_BLOCK]}
          color="red"
          selectedGridType={selectedGridType}
          setSelectedGridType={setSelectedGridType}
        />
      </div>
      <Divider my="md" />
      <Select
        data={ROUGH_PATCH_OPTIONS}
        value={roughPatchOption}
        defaultValue={RoughPatchOption.MIXED}
        onChange={(value) => setRoughPatchInGrid(value as RoughPatchOption)}
        description="Rough patch type"
        checkIconPosition="right"
        clearable={false}
        className="px-4"
      />
      <div className="flex justify-between space-x-4 px-4">
        <NumberInput
          allowDecimal={false}
          hideControls
          min={2}
          value={gridRow}
          onChange={(value) => setGridRow(Number(value))}
          description="Grid row"
        />
        <NumberInput
          allowDecimal={false}
          hideControls
          min={2}
          value={gridColumn}
          onChange={(value) => setGridColumn(Number(value))}
          description="Grid column"
        />
      </div>
      <div className="flex justify-between space-x-4 px-4">
        <NumberInput
          allowDecimal={false}
          hideControls
          value={cellArea}
          onChange={(value) => setCellArea(Number(value))}
          description="Grid Cell Area (m)"
        />
      </div>
      <div className="pb-6 px-4">
        <Text size="xs" c="dimmed">
          Grid cell size
        </Text>

        <Slider
          defaultValue={GridCellSize.MD}
          step={25}
          showLabelOnHover={false}
          marks={GRID_SIZE_MARKS}
          value={gridCellSize}
          onChange={(value) => setGridCellSize(Number(value))}
        />
      </div>
      <div className="text-center">
        <Button
          variant="filled"
          bg="blue"
          onClick={calculateShortestPath}
          disabled={isCalculateDisabled}
          className="w-52"
        >
          Calculate Shortest Path
        </Button>
      </div>
    </div>
  );
};
