import React from "react";
import { Button, Indicator } from "@mantine/core";
import { GridType } from "../../types.ts";
import cx from "classnames";
import { GRID_TYPE_LABEL } from "../../constants.ts";

interface AddGridItemProps {
  totalLeft: number;
  item: GridType;
  image: string;
  color?: string;
  selectedGridType: GridType;
  setSelectedGridType: (item: GridType) => void;
  toggle?: React.ReactNode;
}

export const AddGridItem: React.FC<AddGridItemProps> = ({
  totalLeft,
  item,
  image,
  color = "blue",
  selectedGridType,
  setSelectedGridType,
  toggle,
}) => {
  const isActive = selectedGridType === item;
  const label = isActive
    ? `Adding ${GRID_TYPE_LABEL[item]}`
    : `Add ${GRID_TYPE_LABEL[item]}`;

  return (
    <div className={cx("flex space-x-4 p-4", isActive ? "bg-blue-300" : "")}>
      <img src={image} alt="Building" width={25} />
      <div className="flex justify-between items-center">
        <Indicator label={totalLeft} size={16}>
          <Button
            variant={isActive ? "default" : "outline"}
            fullWidth
            size="xs"
            color={color}
            className="min-w-48"
            disabled={false}
            onClick={() => {
              setSelectedGridType(
                selectedGridType === item ? GridType.EMPTY : item
              );
            }}
          >
            {label}
          </Button>
        </Indicator>
        <div>{toggle}</div>
      </div>
    </div>
  );
};
