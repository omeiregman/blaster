import React, { useMemo } from "react";
import { GridSquare, GridType } from "../../types.ts";

import { GridCell } from "./GridCell.tsx";
import { calculateSelectedGridTypeCount, updateGridCell } from "../../utils";
import {
  useAppContext,
  useAppDispatchContext,
} from "../../store/useAppContext.ts";

export const Grid: React.FC = () => {
  const {
    selectedGridType,
    gridData,
    gridCellSize,
    gridTypeCount,
    gridColumn,
  } = useAppContext();

  const { setGridDataAndCount } = useAppDispatchContext();
  const gridStyle = useMemo(
    () => ({
      display: "inline-grid",
      gridTemplateColumns: `repeat(${gridColumn}, minmax(0, 1fr))`,
    }),
    [gridColumn]
  );

  const isGridClickable = (
    gridType: GridType,
    selectedGridType: GridType
  ): boolean => {
    return (
      (gridType === GridType.EMPTY && selectedGridType !== GridType.EMPTY) ||
      (gridType !== GridType.EMPTY && selectedGridType === GridType.EMPTY)
    );
  };

  const handleGridClick = (x: number, y: number, gridType: GridType) => {
    if (!gridTypeCount[gridType]) {
      return;
    }
    const prevGridType = gridData[y][x].type;
    const updatedGrid = updateGridCell(x, y, gridType, gridData);
    const updatedGridTypeCount = calculateSelectedGridTypeCount(
      gridTypeCount,
      gridType,
      prevGridType
    );
    setGridDataAndCount({
      gridData: updatedGrid,
      gridTypeCount: updatedGridTypeCount,
    });
  };

  return (
    <div className="flex justify-center items-center p-4">
      <div style={gridStyle} className="border-2 border-black">
        {gridData?.map((row, y) =>
          row?.map((square: GridSquare, x) => (
            <GridCell
              key={`${x}-${y}`}
              gridItem={square}
              size={gridCellSize}
              position={{ x, y }}
              onClick={(x, y, type) => handleGridClick(x, y, type)}
              selectedGridType={selectedGridType}
              isGridClickable={isGridClickable(square.type, selectedGridType)}
            />
          ))
        )}
      </div>
    </div>
  );
};
