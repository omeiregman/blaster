import React from "react";
import cx from "classnames";

import BuildingStart from "../../assets/building-start.svg";
import RoadBlock from "../../assets/road-block.svg";
import RoughPatch from "../../assets/rough-patch.svg";
import RoughPatchPass from "../../assets/rough-patch-free.svg";
import { GridSquare, GridType, GridCellSize } from "../../types.ts";

interface GridCellProps {
  gridItem: GridSquare;
  size: GridCellSize;
  onClick: (x: number, y: number, type: GridType) => void;
  position: { x: number; y: number };
  selectedGridType: GridType;
  isGridClickable: boolean;
}

const mapTypeToSvg = (type?: GridType) => {
  switch (type) {
    case GridType.EMPTY:
      return null;
    case GridType.ROAD_BLOCK:
      return RoadBlock;
    case GridType.BUILDING:
      return BuildingStart;
    case GridType.ROUGH_PATCH:
      return RoughPatch;
    case GridType.ROUGH_PATCH_PASS:
      return RoughPatchPass;
    default:
      return null;
  }
};

const mapCellSizeToClass = (cellSize: GridCellSize) => {
  switch (cellSize) {
    case GridCellSize.XS:
      return "w-12 h-12";
    case GridCellSize.SM:
      return "w-16 h-16";
    case GridCellSize.MD:
      return "w-20 h-20";
    case GridCellSize.LG:
      return "w-24 h-24";
    case GridCellSize.XL:
      return "w-28 h-28";
    default:
      return "w-14 h-14";
  }
};

const GridCell = React.memo<GridCellProps>(
  ({
    gridItem,
    size,
    onClick,
    position,
    isGridClickable,
    selectedGridType,
  }: GridCellProps) => {
    const { type: gridType, isPath } = gridItem;
    const gridTypeImage = React.useMemo(
      () => mapTypeToSvg(gridType),
      [gridType]
    );
    const handleClick = () => {
      if (isGridClickable) {
        onClick(position.x, position.y, selectedGridType);
      }
    };

    const cellSizeClass = mapCellSizeToClass(size);
    const cellClass = cx(
      "border-black border-2 cursor-pointer flex justify-center",
      cellSizeClass,
      {
        "bg-green-500": isPath,
      }
    );

    return (
      <div className={cellClass} onClick={handleClick}>
        {gridTypeImage && (
          <img className="w-3/5" src={gridTypeImage} alt={gridType} />
        )}
      </div>
    );
  }
);

GridCell.displayName = "GridCell";

export { GridCell };
