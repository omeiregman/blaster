import {
  GridTypeCount,
  GridCellSize,
  GridType,
  RoughPatchOption,
} from "./types.ts";

export const GRID_SIZE_MARKS = [
  { value: GridCellSize.XS, label: "xs" },
  { value: GridCellSize.SM, label: "sm" },
  { value: GridCellSize.MD, label: "md" },
  { value: GridCellSize.LG, label: "lg" },
  { value: GridCellSize.XL, label: "xl" },
];

export const DEFAULT_SELECTED_GRID_ENTITY = GridType.EMPTY;
export const DEFAULT_GRID_CELL_SIZE = GridCellSize.MD;
export const DEFAULT_CELL_AREA = 250;
export const DEFAULT_GRID_ROW_COLUMN_SIZE = 5;
export const DEFAULT_BUILDING_COUNT = 2;
export const MINIMUM_BUILDING_COUNT = 2;

export const DEFAULT_ROUGH_PATCH_OPTION = RoughPatchOption.MIXED;

export const DEFAULT_GRID_ENTITY_COUNT: GridTypeCount = {
  [GridType.BUILDING]: 0,
  [GridType.ROAD_BLOCK]: 0,
  [GridType.ROUGH_PATCH]: 0,
  [GridType.ROUGH_PATCH_PASS]: 0,
  [GridType.EMPTY]: 0,
};

export const GRID_TYPE_LABEL: Partial<Record<GridType, string>> = {
  [GridType.BUILDING]: "Building",
  [GridType.ROAD_BLOCK]: "Road Block",
  [GridType.ROUGH_PATCH]: "Rough Patch",
  [GridType.ROUGH_PATCH_PASS]: "Rough Patch (pass)",
};

export const ROUGH_PATCH_OPTIONS = [
  {
    value: RoughPatchOption.MIXED,
    label: "Mixed Rough Patch",
  },
  {
    value: RoughPatchOption.BLOCKED,
    label: "Blocked Rough Patch",
  },
  {
    value: RoughPatchOption.UNBLOCKED,
    label: "Unblocked Rough Patch",
  },
];
