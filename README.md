# Blaster - Reg as code task - 
*https://omeiregman.gitlab.io/blaster/*

## Local Project Setup
### Below you will find some information on how to run the application locally.

#### Install Dependencies
```shell
yarn install
```


#### Run the application locally
```shell
yarn run dev
````


#### Open application in local environment
**http://localhost:3000/blaster/**


#### Run tests 
```shell
yarn run test
````


#### Build
```shell
yarn run build
````